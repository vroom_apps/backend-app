package repository

import (
	"context"
	"fmt"
	"server/internal/models"
	"time"

	"github.com/jmoiron/sqlx"
)

type TripsRepo struct {
	db *sqlx.DB
}

func NewTripsRepo(db *sqlx.DB) *TripsRepo {
	return &TripsRepo{
		db: db,
	}
}

func (r *TripsRepo) GetByParameters(ctx context.Context, startRoute, endRoute string, date time.Time, passengersCount int) ([]models.Trip, error) {
	var trips []models.Trip

	query := fmt.Sprintf(`SELECT t.id, t.start_route, t.end_route, t.free_seats, t.cost, t.start_time, t.end_time, t.is_trip_complete, c.id as "car.id", c.car_name as "car.car_name", c.car_number as "car.car_number", c.color as "car.color", c.seats_count as "car.seats_count" FROM %s t JOIN %s c ON t.car = c.id WHERE t.start_route = $1 and t.end_route = $2 and DATE(t.start_time) = DATE($3) AND t.start_time > CURRENT_TIMESTAMP and t.free_seats >= $4`, tripsTable, carsTable)
	err := r.db.Select(&trips, query, startRoute, endRoute, date, passengersCount)
	if err != nil {
		return trips, err
	}

	return trips, nil
}
