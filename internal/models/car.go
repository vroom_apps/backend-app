package models

type Car struct {
	Id         int    `json:"id" db:"id"`
	Name       string `json:"car_name" db:"car_name"`
	Number     string `json:"car_number" db:"car_number"`
	Color      string `json:"color" db:"color"`
	SeatsCount int    `json:"seats_count" db:"seats_count"`
}
