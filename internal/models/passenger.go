package models

type Passenger struct {
	Id          int    `json:"id" db:"id"`
	PhoneNumber string `json:"phone_number" db:"phone_number"`
}
