package main

import (
	"server/internal/app"
)

const configsDir = "configs"

func main() {
	app.Run(configsDir)
}
