package service

import (
	"context"
	"errors"
	"os"
	"server/internal/models"
	"server/internal/repository"
	auth "server/pkg/auth"
	"server/pkg/validator"
	"time"

	"github.com/twilio/twilio-go"
	twilioApi "github.com/twilio/twilio-go/rest/verify/v2"
)

type PassengersService struct {
	repo            repository.Passengers
	tokenManager    auth.TokenManager
	accessTokenTTL  time.Duration
	refreshTokenTTL time.Duration
	twilioClient    *twilio.RestClient
}

func NewPassengersService(
	repo repository.Passengers,
	tokenManager auth.TokenManager,
	accessTTL, refreshTTL time.Duration, twilioClient *twilio.RestClient) *PassengersService {
	return &PassengersService{
		repo:            repo,
		tokenManager:    tokenManager,
		accessTokenTTL:  accessTTL,
		refreshTokenTTL: refreshTTL,
		twilioClient:    twilioClient,
	}
}

func (s *PassengersService) SendOTP(ctx context.Context, input PassengerSendOTPInput) error {
	err := validator.ValidatePhoneNumber(input.PhoneNumber)
	if err != nil {
		return err
	}

	params := &twilioApi.CreateVerificationParams{}
	params.SetTo(input.PhoneNumber)
	params.SetChannel("sms")

	_, err = s.twilioClient.VerifyV2.CreateVerification(os.Getenv("TWILIO_SERVICES_ID"), params)
	if err != nil {
		return err
	}

	return nil
}

func (s *PassengersService) VerifyOTP(ctx context.Context, input PassengerVerifyOTPInput) (Tokens, error) {

	err := validator.ValidatePhoneNumber(input.PhoneNumber)
	if err != nil {
		return Tokens{}, err
	}

	params := &twilioApi.CreateVerificationCheckParams{}
	params.SetTo(input.PhoneNumber)
	params.SetCode(input.Code)

	resp, err := s.twilioClient.VerifyV2.CreateVerificationCheck(os.Getenv("TWILIO_SERVICES_ID"), params)
	if err != nil {
		return Tokens{}, err
	}

	if *resp.Status != "approved" {
		return Tokens{}, errors.New("not a valid code")
	}

	passengerId, err := s.repo.Authenticate(ctx, input.PhoneNumber)
	if err != nil {
		return Tokens{}, err
	}

	return s.createSession(ctx, passengerId)
}

func (s *PassengersService) RefreshTokens(ctx context.Context, refreshToken string) (Tokens, error) {
	passenger, err := s.repo.GetByRefreshToken(ctx, refreshToken)
	if err != nil {
		return Tokens{}, err
	}

	return s.createSession(ctx, passenger.Id)
}

func (s *PassengersService) createSession(ctx context.Context, passengerId int) (Tokens, error) {
	var (
		res Tokens
		err error
	)

	res.AccessToken, err = s.tokenManager.NewJWT(passengerId, s.accessTokenTTL)
	if err != nil {
		return res, err
	}

	res.RefreshToken, err = s.tokenManager.NewRefreshToken()
	if err != nil {
		return res, err
	}

	session := models.Session{
		RefreshToken: res.RefreshToken,
		ExpiresAt:    time.Now().Add(s.refreshTokenTTL),
	}

	err = s.repo.SetSession(ctx, passengerId, session)

	return res, err
}
