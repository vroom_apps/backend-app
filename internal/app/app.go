package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"server/internal/config"
	postgresdb "server/internal/database/postgres"
	"server/internal/handler"
	"server/internal/repository"
	"server/internal/server"
	"server/internal/service"
	"server/pkg/auth"
	"syscall"
	"time"

	_ "github.com/lib/pq"
	"github.com/twilio/twilio-go"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

// Run initializes whole application.
func Run(configPath string) {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	cfg, err := config.Init(configPath)
	if err != nil {
		logrus.Error(err)

		return
	}

	db, err := postgresdb.NewPostgresDB(cfg.Postgres)
	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())

		return
	}

	tokenManager, err := auth.NewManager(cfg.Auth.JWT.SigningKey)
	if err != nil {
		logrus.Error(err)

		return
	}

	twilioClient := twilio.NewRestClientWithParams(twilio.ClientParams{
		Username: cfg.Twilio.TwilioAccountSID,
		Password: cfg.Twilio.TwilioAuthToken,
	})

	repos := repository.NewRepositories(db)

	services := service.NewServices(service.Deps{
		Repos:           repos,
		TokenManager:    tokenManager,
		AccessTokenTTL:  cfg.Auth.JWT.AccessTokenTTL,
		RefreshTokenTTL: cfg.Auth.JWT.RefreshTokenTTL,
		TwilioClient:    twilioClient,
	})

	handlers := handler.NewHandler(services, tokenManager)

	// HTTP Server
	srv := server.NewServer(cfg, handlers.Init(cfg))

	go func() {
		if err := srv.Run(); !errors.Is(err, http.ErrServerClosed) {
			logrus.Errorf("error occurred while running http server: %s\n", err.Error())
		}
	}()

	logrus.Info("Server started")

	// Graceful Shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit

	const timeout = 5 * time.Second

	ctx, shutdown := context.WithTimeout(context.Background(), timeout)
	defer shutdown()

	if err := srv.Stop(ctx); err != nil {
		logrus.Errorf("failed to stop server: %v", err)
	}
}
