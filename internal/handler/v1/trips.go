package v1

import (
	"net/http"
	"server/internal/models"
	"server/internal/service"
	"time"

	"github.com/gin-gonic/gin"
)

func (h *Handler) initTripsRoutes(api *gin.RouterGroup) {
	trips := api.Group("/trips")
	{
		trips.POST("", h.getTrips)
	}
}

type tripParametersInput struct {
	StartRoute      string    `json:"startRoute"`
	EndRoute        string    `json:"endRoute"`
	Date            time.Time `json:"date"`
	PassengersCount int       `json:"passengersCount"`
}

type getTrips struct {
	Data []models.Trip `json:"data"`
}

func (h *Handler) getTrips(c *gin.Context) {
	var inp tripParametersInput
	if err := c.BindJSON(&inp); err != nil {
		newResponse(c, http.StatusBadRequest, "invalid input body")
		return
	}

	trips, err := h.services.Trips.GetByParameters(c.Request.Context(), service.TripParametersInput(inp))
	if err != nil {
		newResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, getTrips{
		Data: trips,
	})
}
