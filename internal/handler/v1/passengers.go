package v1

import (
	"net/http"
	"server/internal/service"

	"github.com/gin-gonic/gin"
)

func (h *Handler) initPassengersRoutes(api *gin.RouterGroup) {
	passengers := api.Group("/passengers")
	{
		passengers.POST("/auth/sendOTP", h.passengerSendOTP)
		passengers.POST("/auth/verifyOTP", h.passengerVerifyOTP)
		passengers.POST("/auth/refresh", h.passengerRefresh)
	}
}

type passengerSendOTPInput struct {
	PhoneNumber string `json:"phoneNumber,omitempty" validate:"required"`
}

func (h *Handler) passengerSendOTP(c *gin.Context) {
	var inp passengerSendOTPInput
	if err := c.BindJSON(&inp); err != nil {
		newResponse(c, http.StatusBadRequest, "invalid input body")
		return
	}

	if err := h.services.Passengers.SendOTP(c.Request.Context(), service.PassengerSendOTPInput(inp)); err != nil {
		newResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusAccepted)
}

type passengerVerifyOTPInput struct {
	PhoneNumber string `json:"phoneNumber,omitempty" validate:"required"`
	Code        string `json:"code,omitempty" validate:"required"`
}

type tokenResponse struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

func (h *Handler) passengerVerifyOTP(c *gin.Context) {
	var inp passengerVerifyOTPInput
	if err := c.BindJSON(&inp); err != nil {
		newResponse(c, http.StatusBadRequest, "invalid input body")

		return
	}

	res, err := h.services.Passengers.VerifyOTP(c.Request.Context(), service.PassengerVerifyOTPInput(inp))
	if err != nil {
		newResponse(c, http.StatusInternalServerError, err.Error())

		return
	}

	c.JSON(http.StatusOK, tokenResponse{
		AccessToken:  res.AccessToken,
		RefreshToken: res.RefreshToken,
	})
}

type refreshInput struct {
	Token string `json:"token" binding:"required"`
}

func (h *Handler) passengerRefresh(c *gin.Context) {
	var inp refreshInput
	if err := c.BindJSON(&inp); err != nil {
		newResponse(c, http.StatusBadRequest, "invalid input body")

		return
	}

	res, err := h.services.Passengers.RefreshTokens(c.Request.Context(), inp.Token)
	if err != nil {
		newResponse(c, http.StatusInternalServerError, err.Error())

		return
	}

	c.JSON(http.StatusOK, tokenResponse{
		AccessToken:  res.AccessToken,
		RefreshToken: res.RefreshToken,
	})
}
