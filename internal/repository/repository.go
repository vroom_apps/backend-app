package repository

import (
	"context"
	"server/internal/models"
	"time"

	"github.com/jmoiron/sqlx"
)

type Passengers interface {
	Create(ctx context.Context, passenger models.Passenger) (int, error)
	GetByCredentials(ctx context.Context, phoneNumber string) (models.Passenger, error)
	GetByRefreshToken(ctx context.Context, refreshToken string) (models.Passenger, error)
	Authenticate(ctx context.Context, phoneNumber string) (int, error)
	SetSession(ctx context.Context, passengerID int, session models.Session) error
}

type Trips interface {
	GetByParameters(ctx context.Context, startRoute, endRoute string, date time.Time, passengersCount int) ([]models.Trip, error)
}

type Repositories struct {
	Passengers Passengers
	Trips      Trips
}

func NewRepositories(db *sqlx.DB) *Repositories {
	return &Repositories{
		Passengers: NewPassengersRepo(db),
		Trips:      NewTripsRepo(db),
	}
}
