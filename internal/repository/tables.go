package repository

const (
	passengerSessionsTable = "passenger_sessions"
	passengersTable        = "passengers"
	tripsTable             = "trips"
	carsTable              = "cars"
)
