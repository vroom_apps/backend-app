package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"server/internal/models"
	"time"

	"github.com/jmoiron/sqlx"
)

type PassengersRepo struct {
	db *sqlx.DB
}

func NewPassengersRepo(db *sqlx.DB) *PassengersRepo {
	return &PassengersRepo{
		db: db,
	}
}

func (r *PassengersRepo) Create(ctx context.Context, passenger models.Passenger) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (phone_number) values ($1) RETURNING id", passengersTable)

	row := r.db.QueryRow(query, passenger.PhoneNumber)
	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

func (r *PassengersRepo) GetByCredentials(ctx context.Context, phoneNumber string) (models.Passenger, error) {
	var passenger models.Passenger

	query := fmt.Sprintf("SELECT * FROM %s WHERE phone_number=$1", passengersTable)
	if err := r.db.Get(&passenger, query, phoneNumber); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Passenger{}, models.ErrPassengerNotFound
		}

		return models.Passenger{}, err
	}

	return passenger, nil
}

func (r *PassengersRepo) Authenticate(ctx context.Context, phoneNumber string) (int, error) {
	var id int

	query := fmt.Sprintf("SELECT id FROM %s WHERE phone_number=$1", passengersTable)
	if err := r.db.Get(&id, query, phoneNumber); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return r.Create(ctx, models.Passenger{PhoneNumber: phoneNumber})
		}

		return 0, err
	}

	return id, nil
}

func (r *PassengersRepo) GetByRefreshToken(ctx context.Context, refreshToken string) (models.Passenger, error) {
	var sessionExpiresAt time.Time
	var passenger models.Passenger

	query := fmt.Sprintf("SELECT expires_at FROM %s WHERE refresh_token=$1", passengerSessionsTable)
	if err := r.db.Get(&sessionExpiresAt, query, refreshToken); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Passenger{}, models.ErrPassengerNotFound
		}

		return models.Passenger{}, err
	}

	if time.Now().After(sessionExpiresAt) {
		query := fmt.Sprintf("DELETE FROM %s WHERE refresh_token=$1", passengerSessionsTable)
		_, err := r.db.Exec(query, refreshToken)

		return models.Passenger{}, err

	}

	query = fmt.Sprintf("SELECT passengers.id, passengers.phone_number FROM %s WHERE Id=(SELECT passenger_id FROM %s WHERE refresh_token=$1)", passengersTable, passengerSessionsTable)
	if err := r.db.Get(&passenger, query, refreshToken); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Passenger{}, models.ErrPassengerNotFound
		}

		fmt.Println(err)

		return models.Passenger{}, err
	}
	return passenger, nil
}

func (r *PassengersRepo) SetSession(ctx context.Context, passengerId int, session models.Session) error {
	query := fmt.Sprintf("INSERT INTO %s (passenger_id, refresh_token, expires_at) values ($1, $2, $3) ON CONFLICT (passenger_id) DO UPDATE SET refresh_token=$2, expires_at=$3", passengerSessionsTable)
	_, err := r.db.Exec(query, passengerId, session.RefreshToken, session.ExpiresAt)

	return err
}
