package postgresdb

import (
	"fmt"

	"server/internal/config"

	"github.com/jmoiron/sqlx"
)

// NewClient established connection to a postgresdb instance using provided URI and auth credentials.
func NewPostgresDB(cfg config.PostgresConfig) (*sqlx.DB, error) {

	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode))

	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}
