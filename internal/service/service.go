package service

import (
	"context"
	"server/internal/models"
	"server/internal/repository"
	auth "server/pkg/auth"
	"time"

	"github.com/twilio/twilio-go"
)

type PassengerSendOTPInput struct {
	PhoneNumber string
}

type PassengerVerifyOTPInput struct {
	PhoneNumber string
	Code        string
}

type Tokens struct {
	AccessToken  string
	RefreshToken string
}

type Passengers interface {
	SendOTP(ctx context.Context, input PassengerSendOTPInput) error
	VerifyOTP(ctx context.Context, input PassengerVerifyOTPInput) (Tokens, error)
	RefreshTokens(ctx context.Context, refreshToken string) (Tokens, error)
}

type TripParametersInput struct {
	StartRoute      string
	EndRoute        string
	Date            time.Time
	PassengersCount int
}

type Trips interface {
	GetByParameters(ctx context.Context, input TripParametersInput) ([]models.Trip, error)
}

type Services struct {
	Passengers Passengers
	Trips      Trips
}

type Deps struct {
	Repos           *repository.Repositories
	TokenManager    auth.TokenManager
	AccessTokenTTL  time.Duration
	RefreshTokenTTL time.Duration
	TwilioClient    *twilio.RestClient
}

func NewServices(deps Deps) *Services {
	passengersService := NewPassengersService(
		deps.Repos.Passengers,
		deps.TokenManager,
		deps.AccessTokenTTL,
		deps.RefreshTokenTTL,
		deps.TwilioClient,
	)

	tripsService := NewTripsService(
		deps.Repos.Trips,
	)

	return &Services{
		Passengers: passengersService,
		Trips:      tripsService,
	}
}
