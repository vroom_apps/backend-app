DROP TABLE booked_tickets;

DROP TABLE passenger_tickets;

DROP TABLE driver_trips;

DROP TABLE tickets;

DROP TABLE trips;

DROP TABLE tickets_points;

DROP TABLE trips_points;

DROP TABLE points;

DROP TABLE cars;

DROP TABLE admin_sessions;

DROP TABLE driver_sessions;

DROP TABLE passenger_sessions;

DROP TABLE admins;

DROP TABLE drivers;

DROP TABLE passengers;