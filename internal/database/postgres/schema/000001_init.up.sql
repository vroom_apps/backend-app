CREATE TABLE
    passengers (
        id serial not null unique,
        phone_number varchar(255) not null unique
    );

CREATE TABLE
    drivers (
        id serial not null unique,
        phone_number varchar(255) not null unique
    );

CREATE TABLE
    admins (
        id serial not null unique,
        phone_number varchar(255) not null unique
    );

CREATE TABLE
    passenger_sessions (
        id serial not null unique,
        passenger_id int references passengers (id) on delete cascade not null unique,
        refresh_token varchar(255) not null unique,
        expires_at timestamp not null
    );

CREATE TABLE
    driver_sessions (
        id serial not null unique,
        driver_id int references drivers (id) on delete cascade not null unique,
        refresh_token varchar(255) not null unique,
        expires_at timestamp not null
    );

CREATE TABLE
    admin_sessions (
        id serial not null unique,
        admin_id int references admins (id) on delete cascade not null unique,
        refresh_token varchar(255) not null unique,
        expires_at timestamp not null
    );

CREATE TABLE
    cars (
        id serial not null unique,
        car_name varchar(255),
        car_number varchar(255) not null,
        color varchar(255),
        seats_count integer not null
    );

CREATE TABLE
    points (
        id serial not null unique,
        point_name varchar(255) not null,
        arrival_time timestamp not null
    );

CREATE TABLE
    trips (
        id serial not null unique,
        start_route varchar(255) not null,
        end_route varchar(255) not null,
        car int references cars (id) on delete cascade not null,
        free_seats integer not null,
        cost integer not null,
        start_time timestamp not null,
        end_time timestamp not null,
        is_trip_complete boolean not null
    );

CREATE TABLE
    tickets (
        id serial not null unique,
        seats_count integer not null,
        total_cost integer not null,
        car int references cars (id) on delete cascade not null,
        is_landing_complete boolean not null
    );

CREATE TABLE
    trips_points (
        id serial not null unique,
        trip_id int references trips (id) on delete cascade not null,
        point_id int references points (id) on delete cascade not null,
        is_loading_point boolean
    );

CREATE TABLE
    tickets_points (
        id serial not null unique,
        tickets_id int references tickets (id) on delete cascade not null,
        point_id int references points (id) on delete cascade not null,
        is_loading_point boolean
    );

CREATE TABLE
    driver_trips (
        id serial not null unique,
        driver_id int references drivers (id) on delete cascade not null,
        trip_id int references trips (id) on delete cascade not null
    );

CREATE TABLE
    passenger_tickets (
        id serial not null unique,
        passenger_id int references passengers (id) on delete cascade not null,
        ticket_id int references tickets (id) on delete cascade not null
    );

CREATE TABLE
    booked_tickets (
        id serial not null unique,
        trip_id int references trips (id) on delete cascade not null,
        ticket_id int references tickets (id) on delete cascade not null
    );