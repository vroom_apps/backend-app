package models

import "time"

type Trip struct {
	Id         int       `json:"id" db:"id"`
	StartRoute string    `json:"start_route" db:"start_route"`
	EndRoute   string    `json:"end_route" db:"end_route"`
	Car        Car       `json:"car"`
	FreeSeats  int       `json:"free_seats" db:"free_seats"`
	Cost       int       `json:"cost" db:"cost"`
	StartTime  time.Time `json:"start_time" db:"start_time"`
	EndTime    time.Time `json:"end_time" db:"end_time"`
	IsComplete bool      `json:"is_trip_complete" db:"is_trip_complete"`
}
