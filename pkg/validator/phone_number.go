package validator

import (
	"regexp"
	"server/internal/models"
)

func ValidatePhoneNumber(phoneNumber string) error {
	// Define a regular expression for phone number validation
	// This regex ensures that the phone number starts with '+', followed by 1 to 15 digits and no other characters.
	regex := `^\+\d{1,15}$`

	// Compile the regular expression
	re := regexp.MustCompile(regex)

	// Use the MatchString method to check if the phone number matches the pattern
	if re.MatchString(phoneNumber) {
		return nil
	}
	return models.ErrPhoneNumberInvalid
}
