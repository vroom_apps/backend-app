package models

import "errors"

var (
	ErrPassengerNotFound  = errors.New("passenger doesn't exists")
	ErrPhoneNumberInvalid = errors.New("phone number is invalid")
)
