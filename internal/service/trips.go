package service

import (
	"context"
	"server/internal/models"
	"server/internal/repository"
)

type TripsService struct {
	repo repository.Trips
}

func NewTripsService(repo repository.Trips) *TripsService {
	return &TripsService{
		repo: repo,
	}
}

func (s *TripsService) GetByParameters(ctx context.Context, input TripParametersInput) ([]models.Trip, error) {
	trips, err := s.repo.GetByParameters(ctx, input.StartRoute, input.EndRoute, input.Date, input.PassengersCount)
	if err != nil {
		return []models.Trip{}, err
	}

	return trips, nil

}
